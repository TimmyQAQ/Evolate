package org.evolate.remapper;

import org.atom.remapper.CatURLClassLoader;

import java.net.URL;
import java.net.URLClassLoader;

public class TransformMethods {
    public static URLClassLoader newInstance(final URL[] urls,
                                             final ClassLoader parent) {
        return new CatURLClassLoader(urls, parent);
    }
}
