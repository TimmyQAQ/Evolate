package org.atom.remapper;

import net.md_5.specialsource.JarMapping;
import net.md_5.specialsource.JarRemapper;
import net.md_5.specialsource.NodeType;
import org.objectweb.asm.Opcodes;

/* Token from CatServer by Atom*/
/* NMS Remap by CatServer*/
public class CatServerRemapper extends JarRemapper {

    public CatServerRemapper(JarMapping jarMapping) {
        super(jarMapping);
    }

    public String mapSignature(String signature, boolean typeSignature) {
        try {
            return super.mapSignature(signature, typeSignature);
        } catch (Exception e) {
            return signature;
        }
    }

    @Override
    public String mapFieldName(String owner, String name, String desc, int access) {
        String mapped = jarMapping.tryClimb(jarMapping.fields, NodeType.FIELD, owner, name, Opcodes.F_NEW);
        return mapped == null ? name : mapped;
    }
}
